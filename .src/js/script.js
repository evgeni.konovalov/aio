;$(document).ready(function(){

	// COUNTER
	(function() {
		// Set the date we're counting down to
		var countDownDate = new Date("October 1, 2018 00:00:00").getTime();

		// Update the count down every 1 second
		var x = setInterval(function() {

			// Get todays date and time
			var now = new Date().getTime();

			// Find the distance between now an the count down date
			var distance = countDownDate - now;

			// Time calculations for days, hours, minutes and seconds
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			// Display the result in the element with id="demo"
			document.getElementById("counter-days").innerHTML = ("00" + days).slice(-2);
			document.getElementById("counter-hours").innerHTML = ("00" + hours).slice(-2);
			document.getElementById("counter-min").innerHTML = ("00" + minutes).slice(-2);
			document.getElementById("counter-sec").innerHTML = ("00" + seconds).slice(-2);

			// If the count down is finished, write some text 
			if (distance < 0) {
				clearInterval(x);
				document.getElementById("counter").innerHTML = "<div class='h4'>Pre-sale has been started!</div>";
			}
		}, 1000);
	})();

	// Modals
	$(document).on('click', '.g-js-modal-open', function(e) {
		e.preventDefault();
		var modal = $(this).data('target');
		$(modal).show(300);
		$('body').addClass('g-noscroll')
	})
	$(document).on('click', '.g-js-modal .close', function(e) {
		console.log('HERE')
		$(this).closest('.g-js-modal').hide(300);
		$('body').removeClass('g-noscroll');
	})

	// Form submit
	var FORM_SELECTOR = '.g-js-form',
		FORM_CLASS_LOADING = 'g-form_loading'
	$(document).on('submit', FORM_SELECTOR + ' form', function(e) {
		e.preventDefault;
		var $form = $(this),
			  $formContainer = $form.closest(FORM_SELECTOR);
			  
		$.post({
	        url: $form.attr('action'),
	        data: $form.serialize(),
	        beforeSend: function() {
	        	$formContainer.addClass(FORM_CLASS_LOADING)
	        },
	        success: function(data){
	        	$formContainer.replaceWith(data);
	        }
	    });
		return false
	})

	// Slider
	$('#problems').slick({
		centerMode: true,
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-caret-left"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-caret-right"></i></button>',
	});

	$('[data-toggle="popover"]').popover({
		trigger: 'hover'
	})

	// Top desktop-menu
	$(window).scroll(function(e){
		var $menu = $('.g-js-fixed-nav'),
			classShow = 'g-visible',
			conditionHeight = $(window).outerHeight() - $menu.outerHeight() - 10;

		if (window.scrollY >= conditionHeight && !$menu.hasClass(classShow)) {
			$menu.addClass(classShow)
		} else if (!(window.scrollY >= conditionHeight) && $menu.hasClass(classShow)) {
			$menu.removeClass(classShow)
		}
	})

	// Mobile-menu
	$(window).scroll(function(e){
		var $menu = $('.g-js-mobile-nav'),
			classShow = 'g-visible',
			conditionHeight = $(window).outerHeight() - $menu.outerHeight() - 10;

		if (window.scrollY >= conditionHeight && !$menu.hasClass(classShow)) {
			$menu.addClass(classShow)
		} else if (!(window.scrollY >= conditionHeight) && $menu.hasClass(classShow)) {
			$menu.removeClass(classShow)
		}
	})

	// Scroll to
	$(document).on('click', '.g-js-scroll-to', function(e) {
		e.preventDefault();
		var $mobileMenu = $('.g-js-mobile-nav'),
			  $fixedMenu = $('.g-js-fixed-nav'),
			  id = $(this).attr('href');

		var offset = $(id).offset().top;

		if ($mobileMenu.length && $mobileMenu.is(':visible')) {
			offset = offset - $mobileMenu.find('.navbar').outerHeight();
			var $toggler = $('.navbar-toggler');
			if (!$toggler.hasClass('collapsed')) {
				$toggler.click()
			}
		} else if ($fixedMenu.length){
			offset = offset - $fixedMenu.outerHeight();
		}
		history.pushState({}, '', location.pathname + id)
	    $('html, body').animate({
	        scrollTop: offset + 5
	    }, 400);
	    return false;
	});

	//Scroll to top
	$(document).on('click', '.g-js-scroll-top', function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, 400);
		return false;
	})

	// Isotop
	var $isoLists = $('.iso-list');
	$isoLists.each(function(i, list){
		var $list = $(list);
		var filtersSelector = $list.data('filters');
		var filterSelector = filtersSelector + ' [data-filter]';

		$list.isotope({
			itemSelector: '#' + $list.attr('id') + '>*',
			layoutMode: 'fitRows',
			filter: $(filterSelector).first().data('filter')
		})

		$(document).on('click', filterSelector, function(e) {
			var filterValue = $(this).data('filter');
			$list.isotope({ filter: filterValue });
			$(filterSelector).removeClass('active');
			$(this).addClass('active');
		})
		
	})

});