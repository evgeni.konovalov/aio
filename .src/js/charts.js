;$(document).ready(function(){

	// Charts
	// Market 1
	var market1 = document.getElementById("market1").getContext('2d');
	var configChartMarket1 = {
	    type: 'bar',
	    data: {
	        labels: ["2016", "2017", "2018", "2019P", "2020P", "2021P", "2022P", "2023P", "2024P"],
	        datasets: 
	        [
		        {
		            label: 'AI Healthcare Market Volume',
		            data: [0.5, 1.3, 1.6, 1.9, 2, 2.4, 4, 5.9, 7.8],
		            backgroundColor: '#3B4D71',
		            borderWidth: 0
		        },
		        {
		            label: 'Other Mediacal Imaging and Diagnosis',
		            data: [0.6, 1.4, 1.8, 2.1, 2.7, 4, 5.5, 7.5, 10],
		            backgroundColor: 'rgb(6, 166, 235)',
		            borderWidth: 0
		        },
		        {
		            label: 'AI Opinion Market Share',
		            data: [0, 0, 1.9, 2.2, 2.9, 4.5, 6.4, 8.5, 12.1],
		            backgroundColor: 'rgb(255, 99, 132)',
		            borderWidth: 0
		        },
	        ]
	    },
	    options: {
	    	legend: {
	    	    onClick: function (e) {
	    	        e.stopPropagation();
	    	    }
	    	},
            scales: {
                xAxes: [{
                    stacked: true,
                    barPercentage: 0.8,
                    gridLines: {
                        display: false
                    },
                }],
                yAxes: [{
                    stacked: false,
                    ticks: {
		                beginAtZero:true
		            },
		            scaleLabel: {
		            	labelString: '$ billions',
		            	display: true,
		            	fontStyle: 'bold',
		            	fontSize: 18
		            }
                }]
            },
            tooltips: {
				position: 'nearest',
				mode: 'index',
				intersect: false,
				callbacks: {
				    label: function(tooltipItem, data) {
				        var label = data.datasets[tooltipItem.datasetIndex].label || '';

				        if (label) {
				            label += ': ';
				        }
				        label += '$' + tooltipItem.yLabel + ' bln.';
				        return label;
				    }
				}
			},
        	maintainAspectRatio: false,
        }
	}

	var chartMarket1, chartMarket1Inited
	function initChartMarket1() {
		chartMarket1 = new Chart(market1, configChartMarket1)
		return true;
	}

	$('#market').waypoint(function(direction) {
		chartMarket1Inited || initChartMarket1() && (chartMarket1Inited = true);
	}, {continuous: false});

	$('#tab-link-market1').on('shown.bs.tab', function (e) {
		initChartMarket1();
	})



	// Market 2

	var market2 = document.getElementById("market2").getContext('2d');
	var configChartMarket2 = {
	    type: 'horizontalBar',
	    data: {
	        labels: [["Artifical", " Intellegence"], ["Internet of", "Things"], "3D Printing", "Robotics", ["Augment", "Reality"], "Blockchain", ["Virtual", "Reality"], "Drones"],
	        datasets: 
	        [
		        {
		            data: [31, 23, 17, 12, 8, 3, 1, 0],
		            backgroundColor: ['rgb(255, 99, 132)'],
		            borderWidth: 0
		        }
	        ]
	    },
	    options: {
            scales: {
                xAxes: [{
                    ticks: {
						min: 0,
						callback: function(value) {
							return value + "%"
						}
					},
                }],
                yAxes: [{
	            	gridLines: {
	            	    display: false
	                },
            	}]
            },
            tooltips: {
	            callbacks: {
	                label: function(tooltipItem, data) {
	                    return tooltipItem.xLabel + '%';
	                }
	            }
	        },
			legend: {
				display: false,
				onClick: function (e) {
				    e.stopPropagation();
				}
			},
        	maintainAspectRatio: false,
        }
	}

	var chartMarket2
	$('#tab-link-market2').on('shown.bs.tab', function (e) {
		chartMarket2 = new Chart(market2, configChartMarket2);
	})



	// Market 3

	var market3 = document.getElementById("market3").getContext('2d');
	var configChartMarket3 = {
	    type: 'horizontalBar',
	    data: {
	    	labels: ["", "", "", "", "", "", "", "", ""],
	        datasets: 
	        [
		        {
		        	label: 'Healthcare would be easier for more people to access',
		            data: [34,0,0,0,0,0,0,0,0],
		            backgroundColor: '#ef9a9a',
		            borderWidth: 0
		        },
		        {
		        	label: 'Faster and more accurate diagnoses',
		            data: [0,31,0,0,0,0,0,0,0],
		            backgroundColor: '#ce93d8',
		            borderWidth: 0
		        },
		        {
		        	label: 'Will make better treatment recommendations',
		            data: [0,0,27,0,0,0,0,0,0],
		            backgroundColor: '#90caf9',
		            borderWidth: 0
		        },
		        {
		        	label: ['Like having your own healthcare specialist,', 'available at any time'],
		            data: [0,0,0,27,0,0,0,0,0],
		            backgroundColor: '#80cbc4',
		            borderWidth: 0
		        },
		        {
		        	label: 'Fewer mistake than doctors or healthcare professionals',
		            data: [0,0,0,0,20,0,0,0,0],
		            backgroundColor: '#a5d6a7',
		            borderWidth: 0
		        },
		        {
		        	label: 'Can perform surgery and diagnostic tests accurately than humans',
		            data: [0,0,0,0,0,17,0,0,0],
		            backgroundColor: '#fff59d',
		            borderWidth: 0
		        },
		        {
		        	label: 'None of these',
		            data: [0,0,0,0,0,0,4,0,0],
		            backgroundColor: '#eeeeee',
		            borderWidth: 0
		        },
		        {
		        	label: "Don't know",
		            data: [0,0,0,0,0,0,0,13,0],
		            backgroundColor: '#e0e0e0',
		            borderWidth: 0
		        },
		        {
		        	label: 'Not applicable - no advantages',
		            data: [0,0,0,0,0,0,0,0,17],
		            backgroundColor: '#bdbdbd',
		            borderWidth: 0
		        },
		        
	        ]
	    },
	    options: {
	    	legend: {
	    		onClick: function (e) {
	    		    e.stopPropagation();
	    		},
	    		labels: {
	    			fontSize: 10,
	    		}
	    	},
            scales: {
                xAxes: [{
                    ticks: {
						min: 0,
						callback: function(value) {
							return value + "%"
						},
					},
                }],
                yAxes: [{
                	gridLines: {
                	    display: false
                	},
                	stacked: true,
                	// barThickness: 30,
                	// barPercentage: 1,
                }]
            },
            tooltips: {
            	mode: 'point',
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';

                        if (label) {
                            label += ': ';
                        }
                        label += tooltipItem.xLabel + '%';
                        return label;
                    }
                }
            },
            maintainAspectRatio: false,
        }
	}
	var chartMarket3
	$('#tab-link-market3').on('shown.bs.tab', function (e) {
		chartMarket3 = new Chart(market3, configChartMarket3);
	})



	// Indicators 2

	var timeFormat = 'MM/YYYY';
	var indicators2 = document.getElementById("indicators2").getContext('2d');
	var configChartIndicators2 = {
	    type: 'line',
	    data: {
	        labels: [],
	        datasets: 
	        [
		        {
		            backgroundColor: 'rgba(6, 166, 235, .5)',
		            data: [
		            	{x: '07/2017', y: '0'},
		            	{x: '08/2017', y: '50'},
		            	{x: '09/2017', y: '50'},
		            	{x: '10/2017', y: '35'},
		            	{x: '11/2017', y: '40'},
		            	{x: '12/2017', y: '45'},
		            	{x: '01/2018', y: '120'},
		            	{x: '02/2018', y: '120'},
		            	{x: '03/2018', y: '120'},
		            	{x: '04/2018', y: '270'},
		            	{x: '05/2018', y: '400'},
		            	{x: '06/2018', y: '610'},
		            	{x: '07/2018', y: '880'},
		            	{x: '08/2018', y: '1400'},
		            	{x: '09/2018', y: '1650'},
		            	{x: '10/2018', y: '1820'},
		            	{x: '11/2018', y: '2500'},
		            	{x: '12/2018', y: '2550'},
		            	{x: '01/2019', y: '2700'},
		            	{x: '02/2019', y: '2800'},
		            	{x: '03/2019', y: '3100'},
		            	{x: '06/2019', y: '3400'},
		            ]
		        }
	        ]
	    },
	    options: {
	    	elements: {
                point:{
                    radius: 0
                }
            },
	    	tooltips: {
				position: 'nearest',
				intersect: false,
			},
            scales: {
                xAxes: [{
                	type: 'time',
                	time: {
                		parser: timeFormat,
                	},
                }]
            },
        	maintainAspectRatio: false,
			legend: {
				display: false,
				onClick: function (e) {
				    e.stopPropagation();
				}
			},
        }
	};

	var chartIndicators2
	$('#tab-link-indicators2').on('shown.bs.tab', function (e) {
		chartIndicators2 = new Chart(indicators2, configChartIndicators2);
	})



	// Indicators 3

	var indicators3 = document.getElementById("indicators3").getContext('2d');
	var configChart3 = {
	    type: 'radar',
	    data: {
	        labels: [['Blood', 'Cells'], ['Ocular', 'Fundus'], 'CT/MRI', ['Lungs', 'Ultrasound'], 'Mammography', ['Urinary', 'Bladder', 'Ultrasonography'], ['Female', 'Reproductive', 'System']],
	        datasets: 
	        [
		        {
		            backgroundColor: 'rgba(6, 166, 235, .5)',
		            data: [4, 1, 0.5, 0.7, 0, 0, 0]
		        }
	        ]
	    },
	    options: {
	    	elements: {
                point:{
                    radius: 0
                }
            },
            scale: {
                ticks: {
                	min: 0,
                	max: 10,
                	display: false
                },
				gridLines: {
					color: 'rgba(0,0,0,0.03)'
				},
				angleLines: {
					display: false
				},
            },
        	maintainAspectRatio: false,
			legend: {
				display: false,
				onClick: function (e) {
				    e.stopPropagation();
				}
			},
			tooltips: {
				enabled: false
			}
        }
	}
	var chartIndicators3 = new Chart(indicators3, configChart3);



	var interval = 1300;
	var timer;

	function clickNextRadar(num) {
		return function(){
			num = $('.js-radar').length - 1 >= num ? num : 0;
			$('.js-radar')[num].click()
			timer = setTimeout(clickNextRadar(num + 1), interval)
		}
	}

	$(document).on('click', '.js-radar', function(e) {
		configChart3.data.datasets[0].data = $(this).data('points');
		chartIndicators3.update();
		$('.js-radar').removeClass('active');
		$(this).addClass('active');
		if (e.hasOwnProperty('originalEvent')) {
			clearTimeout(timer)
		}
	})

	$('#tab-link-indicators3').on('shown.bs.tab', function (e) {
		clickNextRadar(0)();
	})
	$('#tab-link-indicators3').on('hidden.bs.tab', function (e) {
		clearTimeout(timer)
	})

	var canvasTokens = document.getElementById("chart-tokens").getContext('2d');
	var configChartTokens = {
	    type: 'doughnut',
	    data: {
	        labels: ['Tokens for Sale', 'Tokens for Founders and Employee*', 'Tokens for a Reserve Fund', 'Tokens for Bounty Program'],
	        datasets: 
	        [
		        {
		            backgroundColor: ['#ED2A61', '#48AFDB', '#6D74C0', '#FF7B29'],
		            data: [50, 25, 23, 2]
		        }
	        ]
	    },
	    options: {
        	maintainAspectRatio: false,
			legend: {
				position: 'right',
				labels: {
					boxWidth: 15,
					fontColor: '#fff'
				},
				onClick: function (e) {
				    e.stopPropagation();
				}
			},
			tooltips: {
				callbacks: {
	                label: function(tooltipItem, data) {
	                    var label = data.labels[tooltipItem.index] || '';

	                    if (label) {
	                        label += ': ';
	                    }
	                    label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
	                    return label;
	                }
	            }
        	}
        }
	}
	var chartTokens
	$('#tokens-placement').waypoint(function(direction) {
		chartTokens || (chartTokens = new Chart(canvasTokens, configChartTokens));
	}, {
		offset: '70%'
	});


	var canvasFunds = document.getElementById("chart-funds").getContext('2d');
	var configChartFunds = {
	    type: 'horizontalBar',
	    data: {
	        labels: [["Purchase", "Medical Data"], "Marketing", ["Managers and", "Acquisitions"], "AI Development", ["API", "Development"]],
	        datasets: 
	        [
		        {
		            data: [64, 15, 10, 6, 5],
		            backgroundColor: '#ffffff',
		            borderWidth: 0
		        }
	        ]
	    },
	    options: {
            scales: {
                xAxes: [{
                    ticks: {
						min: 0,
						callback: function(value) {
							return value + "%"
						}
					},
					ticks: {
						fontColor: '#fff'
					}
                }],
                yAxes: [{
	            	gridLines: {
	            	    display: false
	                },
	                ticks: {
	                	fontColor: '#fff'
	                }
            	}]
            },
            tooltips: {
	            callbacks: {
	                label: function(tooltipItem, data) {
	                    return tooltipItem.xLabel + '%';
	                }
	            },
	            displayColors: false
	        },
			legend: {
				display: false,
				onClick: function (e) {
				    e.stopPropagation();
				}
			},
        	maintainAspectRatio: false,
        }
	};

	var chartFunds
	$('#use-of-funds').waypoint(function(direction) {
		chartFunds || (chartFunds = new Chart(canvasFunds, configChartFunds));
	}, {
		offset: '70%'
	});

});