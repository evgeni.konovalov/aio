var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var del = require('del');
var gutil = require('gulp-util');

BASE_SRC = './.src/';
BASE_DEST = './webapp/';

paths = {
    css: {
        src: BASE_SRC + 'sass/**/*.scss',
        dest: BASE_DEST + 'css/'
    },
    scripts: {
        bundles: [
            {
                name: 'vendor',
                minified: true,
                src: [
                    'node_modules/jquery/dist/jquery.min.js',
                    'node_modules/popper.js/dist/umd/popper.min.js',
                    'node_modules/bootstrap/dist/js/bootstrap.min.js',
                    'node_modules/chart.js/dist/Chart.bundle.min.js',
                    'node_modules/waypoints/lib/jquery.waypoints.min.js',
                    'node_modules/isotope-layout/dist/isotope.pkgd.min.js'
                ]
            },
            {
                name: 'script',
                src: [
                    BASE_SRC + 'js/script.js',
                    BASE_SRC + 'js/charts.js'
                ]
            }
        ],
        src: BASE_SRC + 'js/**/*.js',
        dest: BASE_DEST + 'js/',
    },
}

function css() {
  return gulp.src(paths.css.src)
      .pipe(sass().on('error', sass.logError))
      .pipe(cleanCSS())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest(paths.css.dest))
}
exports.css = css;

function scripts(done) {
    paths.scripts.bundles.forEach(function(bundle, i) {
        const isLast = i + 1 == paths.scripts.bundles.length;
        gulp.src(bundle.src, { sourcemaps: true })
            .pipe(gulpif(!bundle.minified, uglify()))
            .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
            .pipe(concat(bundle.name + '.min.js'))
            .pipe(gulp.dest(paths.scripts.dest))
        isLast && done();
    })
}
exports.scripts = scripts;

function watch() {
    gulp.watch(paths.scripts.src, scripts);
    gulp.watch(paths.css.src, css);
}
exports.watch = watch;

const build = gulp.parallel(scripts, css);
exports.default = build;